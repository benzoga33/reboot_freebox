
####################
# Reboot Freebox   #
####################

Script pour rebooter la freebox à en cas de perte de connexion 
lorsqu'elle est configurée en mode bridge avec un routeur autonome aux "fesses".

source git clone https://github.com/JrCs/freeboxos-bash-api

1/ Editer "freeboxos-bash-api.sh"

FREEBOX_URL= »mafreebox.free.fr »
remplacé par:
FREEBOX_URL= »http://212.27.38.253″

Attention, $FREEBOX_URL en mettant l’@ip en dur de la freebox. 
Même en mode bridge la freebox est joignable via une ip interne, pratique en cas de perte du service dns. 
Pour l’obtenir faire un :
ping -c mafreebox.freebox.fr

2/ Autoriser le script "rebootFbox.sh" à utiliser l'API:
- taper dans un terminal:
authorize_application 'rebootFbox.sh' 'script reboot freebox' '1.0.0' 'rpi'
- sur l'écran de la freebox, valider avec le bouton  "fleche droite"
- relever "MY_APP_ID" et "MY_APP_TOKEN" et les copier dans "rebootFbox.sh"

3/ Autoriser la modification des settings par le script (case à cocher)
- Sur http://mafreebox.free.fr

4/ test de reboot
- taper dans un terminal:
login_freebox "$MY_APP_ID" "$MY_APP_TOKEN"
puis
reboot_freebox

5/ PLanifier 
- Editer la crontab , pour une exécution toutes les 4 mn:
*/4 * * * * /root/reboot_freebox/rebootFbox.sh

