#!/bin/bash

# Adresse de la passerelle Internet Free
GATEWAY="82.64.8.254"
flagTest="/tmp/flagFailFbox.txt"
DATE=`date '+%Y%m%d'`
HEURE=$(date +%H:%M)

# API infos
MY_APP_ID="rebootFbox.sh"
MY_APP_TOKEN="monSuperTokengQ4YPlF1p2ZAl"
LOG="/tmp/rebootFbox.log"

##################################################################################
# verif temoin de reboot pr notifier  par mail
if [ -f "/tmp/reboot.txt" ];
then
	cat $LOG |mail -s "Echec test de connectivité - la freebox a redémarré"  monmail@coucou.fr
	rm /tmp/reboot.txt
fi

touch $LOG
>$LOG

# on charge les libs 
source freeboxos_bash_api.sh

# premier test
/bin/ping -c 1 $GATEWAY &>/dev/null
if (( $? != 0 )) ; then
	touch $flagTest
	echo -e "$DATE $HEURE">>$LOG
	echo "echec connexion $GATEWAY">>$LOG
else
	#"Passerelle répond au ping."
	# on sort du script
	exit 0
fi

# Second test - Vérification de la présence de la Freebox

if [ -f "$flagTest" ] ; then
	/bin/ping -c 1 mafreebox.freebox.fr &>/dev/null
	if (( $? != 0 )) ; then
		echo -e "$DATE $HEURE">>$LOG
		echo "echec connexion domaine mafreebox.freebox.fr">>$LOG
		echo "reboot de la box!">>$LOG
		rm "$flagTest"
		# redémarrage de la Freebox
		touch /tmp/reboot.txt
		login_freebox "$MY_APP_ID" "$MY_APP_TOKEN"
		reboot_freebox
		exit 0
	fi
fi


